﻿using System;
using System.Collections.Generic;

namespace Sasw.PatronEstrategia
{
    class Program
    {
        static void Main(string[] args)
        {
            var dwarf = new Dwarf(new Kicker());
            var boxer = new Boxer(new Puncher());
            var zombie = new Zombie(new Biter());
            var kangaroo = new Kangaroo(new Kicker());

            var warriors = new List<Warrior> { dwarf, boxer, zombie, kangaroo };

            // DO NOT TOUCH THIS
            foreach (var warrior in warriors)
            {
                Console.WriteLine($"Aparece un guerrero {warrior.GetType().Name}");
                warrior.ShowYourMove();
                Console.WriteLine();
            }
            // END OF DO NOT TOUCH THIS
            
            Console.WriteLine($"apocalipsis Zombie!");
            dwarf.ChangeFightingBehavior(new Biter());
            boxer.ChangeFightingBehavior(new Biter());
            
            foreach (var warrior in warriors)
            {
                Console.WriteLine($"Aparece un guerrero {warrior.GetType().Name} y ahora..");
                warrior.ShowYourMove();
                Console.WriteLine();
            }
        }
    }

    public class Warrior
    {
        private IFightingBehavior _fightingBehavior;

        protected Warrior(IFightingBehavior fightingBehavior)
        {
            _fightingBehavior = fightingBehavior;
        }

        public void ShowYourMove()
        {
            _fightingBehavior.Fight();
        }

        public void ChangeFightingBehavior(IFightingBehavior fightingBehavior)
        {
            _fightingBehavior = fightingBehavior;
        }
    }

    public class Dwarf
        : Warrior
    {
        public Dwarf(IFightingBehavior fightingBehavior) 
            : base(fightingBehavior)
        {
        }
    }

    public class Boxer
        : Warrior
    {
        public Boxer(IFightingBehavior fightingBehavior) 
            : base(fightingBehavior)
        {
        }
    }

    public class Zombie
        : Warrior
    {
        public Zombie(IFightingBehavior fightingBehavior) 
            : base(fightingBehavior)
        {
        }
    }
    
    public class Kangaroo
        : Warrior
    {
        public Kangaroo(IFightingBehavior fightingBehavior) 
            : base(fightingBehavior)
        {
        }
    }
}