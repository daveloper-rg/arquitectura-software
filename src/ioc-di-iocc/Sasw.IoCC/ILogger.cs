namespace Sasw.IoCC
{
    public interface ILogger
    {
        void Log(string message);
    }
}