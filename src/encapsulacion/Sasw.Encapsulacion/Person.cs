using System;

namespace Sasw.Encapsulacion
{
    public class Person
    {
        private string Name { get; }
        private int Age { get; }
        private PhysicalState PhysicalState { get; set; } = PhysicalState.Thirsty;
        
        public Person(string name, int age)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            if (age < 0 || age > 130)
            {
                throw new ArgumentException("Not a valid age for a person");
            }
            Age = age;
        }

        public void Drink(Drink drink)
        {
            if (PhysicalState == PhysicalState.Good)
            {
                // no need to drink
                return;
            }
            
            if (drink == Encapsulacion.Drink.Whiskey)
            {
                if (Age < 18)
                {
                    throw new Exception("I'm underage and I can't drink whiskey");
                }
                
                PhysicalState = PhysicalState.AlcoholDrunk;
                return;
            }
            
            if (drink == Encapsulacion.Drink.Water && Age < 1)
            {
                throw new Exception("I am a baby and I can't drink water, just milk");
            }

            PhysicalState = PhysicalState.Good;
        }

        public override string ToString()
        {
            return $"I'm {Name} and I am {PhysicalState}";
        }
    }
}