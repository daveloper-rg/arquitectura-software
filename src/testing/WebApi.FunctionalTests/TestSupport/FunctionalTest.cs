using System.Net.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using WebApi.Contracts;
using WebApi.FunctionalTests.TestSupport.Extensions;
using WebApi.Services;

namespace WebApi.FunctionalTests.TestSupport
{
    public abstract class FunctionalTest
        : Given_When_Then_Test_Async
    {
        protected HttpClient HttpClient { get; }
        
        protected FunctionalTest()
        {
            var server =
                new TestServer(
                    new WebHostBuilder()
                        .UseStartup<Startup>()
                        .UseCommonConfiguration()
                        .UseEnvironment("Test")
                        .ConfigureTestServices(ConfigureTestServices));

            HttpClient = server.CreateClient();
        }

        protected virtual void ConfigureTestServices(IServiceCollection services)
        {
            //services.Replace<ITeamService>(sp => new InMemoryTeamService(), ServiceLifetime.Singleton);
        }
    }
}