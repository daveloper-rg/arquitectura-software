using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Contracts
{
    public interface ITeamService
    {
        Task<IEnumerable<Team>> GetTeams();
    }
}