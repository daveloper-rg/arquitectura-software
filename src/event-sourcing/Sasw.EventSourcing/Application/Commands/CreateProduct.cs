using Sasw.EventSourcing.Application.Contracts;

namespace Sasw.EventSourcing.Application.Commands
{
    public class CreateProduct
        : ICommand
    {
        public string Name { get; set; }
    }
}