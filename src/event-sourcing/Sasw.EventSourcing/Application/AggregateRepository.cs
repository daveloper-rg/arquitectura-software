using System;
using Sasw.EventSourcing.Application.Contracts;
using Sasw.EventSourcing.Domain;

namespace Sasw.EventSourcing.Application
{
    public class AggregateRepository<TAggregateRoot>
        : IAggregateRepository<TAggregateRoot>
        where TAggregateRoot : AggregateRoot
    {
        private readonly IEventStore _eventStore;
        private readonly AggregateRootFactory<TAggregateRoot> _aggregateRootFactory;

        public AggregateRepository(
            IEventStore eventStore,
            AggregateRootFactory<TAggregateRoot> aggregateRootFactory)
        {
            _eventStore = eventStore;
            _aggregateRootFactory = aggregateRootFactory;
        }
        
        public TAggregateRoot Get(Guid aggregateId)
        {
            var domainEvents = _eventStore.GetEvents(aggregateId.ToString());
            var aggregateRoot = _aggregateRootFactory.Create(aggregateId, domainEvents);
            return aggregateRoot;
        }
    }
}