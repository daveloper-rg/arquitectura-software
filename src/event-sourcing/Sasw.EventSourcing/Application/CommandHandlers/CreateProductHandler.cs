using System;
using Sasw.EventSourcing.Application.Commands;
using Sasw.EventSourcing.Application.Contracts;
using Sasw.EventSourcing.Domain.Contracts;
using Sasw.EventSourcing.Domain.ProductAggregate;

namespace Sasw.EventSourcing.Application.CommandHandlers
{
    public class CreateProductHandler
        : ICommandHandler<CreateProduct>
    {
        private readonly IDomainEventsConsumer _domainEventsConsumer;

        public CreateProductHandler(IDomainEventsConsumer domainEventsConsumer)
        {
            _domainEventsConsumer = domainEventsConsumer;
        }
        
        public Guid Handle(CreateProduct createProduct)
        {
            var newGuid = Guid.NewGuid();
            var product = new Product(newGuid, createProduct.Name);
            product.ConsumeDomainEventChanges(_domainEventsConsumer);
            return newGuid;
        }
    }
}