using System;

namespace Sasw.EventSourcing.Domain.Contracts
{
    public interface IDomainEvent
    {
        Guid AggregateId { get; }
    }
}