using System;
using Sasw.EventSourcing.Domain.Contracts;

namespace Sasw.EventSourcing.Domain.ProductAggregate.Events
{
    public class PriceSet
        : IDomainEvent
    {
        public Guid AggregateId { get; }
        public string OriginalCurrency { get; }
        public decimal OriginalAmount { get; }
        public decimal AmountInUsd { get; }
        public decimal ExchangeRate { get; }

        public PriceSet(
            Guid aggregateId,
            string originalCurrency, 
            decimal originalAmount, 
            decimal amountInUsd, 
            decimal exchangeRate)
        {
            AggregateId = aggregateId;
            OriginalCurrency = originalCurrency;
            OriginalAmount = originalAmount;
            AmountInUsd = amountInUsd;
            ExchangeRate = exchangeRate;
        }
    }
}